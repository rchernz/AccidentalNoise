﻿namespace RcherNZ.AccidentalNoise
{
    public enum MappingMode
    {
        SeamlessNone,
        SeamlessX,
        SeamlessY,
        SeamlessZ,
        SeamlessXY,
        SeamlessXZ,
        SeamlessYZ,
        SeamlessXYZ
    }
}

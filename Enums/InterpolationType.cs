namespace RcherNZ.AccidentalNoise
{
    public enum InterpolationType
    {
        None,
        Linear,
        Cubic,
        Quintic
    }
}
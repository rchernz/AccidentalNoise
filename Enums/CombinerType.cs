namespace RcherNZ.AccidentalNoise
{
    public enum CombinerType
    {
        Add,
        Multiply,
        Max,
        Min,
        Average
    }
}
namespace RcherNZ.AccidentalNoise
{
    public enum FractalType
    {
        FractionalBrownianMotion,
        RidgedMulti,
        Billow,
        Multi,
        HybridMulti
    }
}
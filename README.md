# AccidentalNoise

A modified C# port of the [http://accidentalnoise.sourceforge.net/](Accidental Noise Library) by Joshua Tippett
Forked from [https://github.com/TinkerWorX/AccidentalNoiseLibrary](TinkerWorX's GitHub Repo) 

## About
This fork was made to create a .NET Standard NuGet package that can easily be added to any .NET project.

No additional changes have been made from the GitHub repo it was originally forked from, other than implementing an
automatic build system for NuGet

## Notes from original author
It is still in need of some proper C# specific optimizations and the Color modules are missing for the time being. 
Included is a special XML reading module which is also work in progress.
